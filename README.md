# TB Platform #

The aim of this project is to create a platform (website) that can be used to fill, store and display sports predictions, particularly regarding knockout stages of a competition. It accepts full-bracket predictions, and predictions on single series. It also computes the "penalty" score associated to every prediction, against a "reference" prediction.

The project started as a way to move online an annual prediction game on the NBA Playoffs, that I do with some friends of mine. For the moment, the aim of the project remains purely entertaining, and the scope is small groups rather than the large public. Therefore, I am keeping the project easy-to-use, without adding too many functionalities.

The current version is built with Ruby using the Sinatra mini-framework, and is backed by an SQLite database. This allows for fine control of the whole system, without the use of heavy frameworks. Since the current goal of the project is to provide something simple, it relies on the web server WEBrick, which is built-in in Ruby. There are no scaling issues, as I am not even expecting double-digit concurrent requests.

The platform is suitable for its immediate deployment to hosting services. I have succesfully deployed it to RedHar OpenShift Online (with the starter plan).

## Deployment instructions ##

I have tested it only with RedHat OpenShift, which uses Docker. Start a new app in this way:

    $ oc new-app https://gitlab.com/cucno/tb-platform.git --name=NAME

This triggers a webhook that recognizes that we are using ruby and reads all the configuration files. It initializes a BuildConfiguration and a DeploymentConfiguration, which are used as instructions to build and deploy the app. You can see that the system is initializing an starting the first build with

    $ oc status
    $ oc logs -f build/NAME-1

We now want to add a persistent storage, i.e. disk space which is not reset at every build. This is required if we want to use a database which is carried over between different versions of the app. Without a persistent storage, the platform defaults to a in-memory database, which is lost at every new build: this is fine for testing, but not for production.

First of all, we have to tell the system to deploy new versions with the "Recreate" strategy, instead of the default "Rolling". Rolling means that the old pod (i.e. old deployment) is stopped when the new one is ready, while Recreate means that the new one is started when the old one has stopped. The reason we need Recreate is that with the free tier, persistent volumes cannot be shared between pods, so if the new one starts before the old one has stopped, it will not be able to mount the volume with the database, throwing errors and going into a loopback crash. So, we edit the DeploymentConfiguration with

    $ oc edit dc/NAME

This is a YAML file: we have to change the value in "spec -> strategy -> type: Rolling" to "Recreate".

Now we can claim a persistent volume and add it to the DeploymentConfiguration:

    $ oc set volume dc --add --type=persistentVolumeClaim --claim-name=data --claim-size=1Gi --mount-path /data

We now set the environment to production with a global variable

    $ oc set env dc/NAME APP_ENV='production'

Finally, we can expose the service:

    $ oc expose dc NAME --port=8080

The status of the project can be seen with

    $ oc status
    $ oc get pods
    $ oc get routes


## Technologies ##

Ruby 2.5, Sinatra 2.0.4 with Sinatra-contrib, Sqlite3 1.3.13, Sequel 5.12.0
