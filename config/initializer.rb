require 'yaml'

#Initialize teams' official 3-letters abbreviations
ABBR = {'ATL': 'Atlanta Hawks',
'BKN': 'Brooklyn Nets',
'BOS': 'Boston Celtics',
'CHA': 'Charlotte Hornets',
'CHI': 'Chicago Bulls',
'CLE': 'Cleveland Cavaliers',
'DAL': 'Dallas Mavericks',
'DEN': 'Denver Nuggets',
'DET': 'Detroit Pistons',
'GSW': 'Golden State Warriors',
'HOU': 'Houston Rockets',
'IND': 'Indiana Pacers',
'LAC': 'Los Angeles Clippers',
'LAL': 'Los Angeles Lakers',
'MEM': 'Memphis Grizzlies',
'MIA': 'Miami Heat',
'MIL': 'Milwaukee Bucks',
'MIN': 'Minnesota Timberwolves',
'NOP': 'New Orleans Pelicans',
'NYK': 'New York Knicks',
'OKC': 'Oklahoma City Thunder',
'ORL': 'Orlando Magic',
'PHI': 'Philadelphia 76ers',
'PHX': 'Phoenix Suns',
'POR': 'Portland Trail Blazers',
'SAC': 'Sacramento Kings',
'SAS': 'San Antonio Spurs',
'TOR': 'Toronto Raptors',
'UTA': 'Utah Jazz',
'WAS': 'Washington Wizards'}

#Initialize weights for predictions malus
WEIGHT = [0,1,1.4,2]
SWEEP_BONUS = -0.5

SEEDS = *(1..8) #range from 1 to 8
matchup_order = [1,8,4,5,2,7,3,6]

results = YAML::load_file('config/results.yaml')

#Initialize teams from the config file in the first run of the webapp
TEAMS = {}
teams = results['teams']

if DB[:Teams].empty?
    teams.each { |conference, tt|
        newlist = []
        tt.each { |data, n|
            newteam = Team.create(name: data['name'], seed: data['seed'], conference: conference)
            newlist.append(newteam)
        }
        TEAMS[conference] = newlist.sort_by { |team| matchup_order.index team.seed }
    }
end

if DB[:Brecords].empty?
    teams.each { |conference, tt|
        tt.each { |data|
            Brecord.create(team: data['name'], wins: data['wins'], user_id: 1)
        }
    }
end
