require 'sinatra'
require 'sinatra/reloader'

require_relative 'models/init'
require_relative 'config/initializer'

set :bind, '0.0.0.0'
set :port, 8080
enable :sessions

def authorize_user(username,password)
    user = User.where(name: username).first
    if user.password == password
        return user
    else
        return nil
    end
end

def filter_dates(array)
    newar = []
    array.each { |matchup|
        if (matchup.deadline != nil) and (matchup.deadline > Time.now)
            newar.append(matchup)
        end
    }
    return newar
end

#Function that validates a prediction
def validate_prediction(predictions) #predictions is an array of numbers from 0 to 4
    for i in (0...predictions.length) do
        if (i % 3) == 0 #the validation is based on triples of consecutive numbers (wins1, wins2, round) so the index must be divisible by three otherwise we do nothing
            if predictions[i] < 4 and predictions[i+1] < 4 then #someone has to win
                return false
            elsif predictions[i] == predictions[i+1] then #only on team has to win (here we now one is 4, so if they're equal they're both 4)
                return false
            end
        end
    end
    return true #if all the predictions are validated, we return true
end

#Function that stores a prediction
def store_predictions(predictions,user_id)
    for i in (0...predictions.length) do
        if (i % 3) == 0 #the validation is based on triples of consecutive numbers (wins1, wins2, round) so the index must be divisible by three otherwise we do nothing
            Prediction.create(team1: predictions.keys[i], team2: predictions.keys[i+1], wins1: predictions.values[i], wins2: predictions.values[i+1], round: predictions.values[i+2], user_id: user_id)
        end
    end
end

#Function that stores bracket records
def store_brecords(brecords,user_id)
    brecords.each { |record|
        records_hash = {team: record[0], wins: record[1], user_id: user_id}
        Brecord.create(records_hash)
    }
end

#Index page is static
get '/' do
    redirect '/index.html'
end

#With this GET they receive the page to fill in the bracket. Users must choose the total number of wins that a team will get, so they have to assign to any team an integer. We thus need as instance variable all the teams in the playoffs.
get '/brecord/new' do
    @message = session.delete(:message) #stores an error message (if present) in @message to show to the user. It also deletes it from session.
    @teams = Team.all #retrieve all the teams that take part in the playoffs
    erb :'brecords/new'
end

#When they choose a name and submit it, we redirect them to the predictions page
post '/brecord/new' do
    @brecords = params[:data] #this variable contains the values submitted in the form of the kind {"Lakers" => "4"}
    username = params[:username]
    password = params[:password]

    if user = authorize_user(username,password)
        store_brecords(@brecords,user.id)
        erb :confirmation
    else
        session[:message] = "Invalid password"
        redirect "/prediction/new"
    end
end

#With this GET they receive the page to do the predictions
get '/prediction/new' do
    @message = session.delete(:message) #stores an error message (if present) in @message to show to the user. It also deletes it from session.
    @matchups = Prediction.where(user_id: 1)
    @matchups = filter_dates(@matchups)
    erb :'predictions/new'
end

#When they post a prediction, we validate it. If it's ok we display a confirmation page
post '/prediction/new' do
    @predictions = params[:data] #this variable contains the values submitted in the form
    username = params[:username]
    password = params[:password]
    if validate_prediction(@predictions.values.map(&:to_i)) then #predictions contains elements of the form {"Lakers" => "4"}, so we validate them based solely on the values. We also transform them into integers
        if user = authorize_user(username,password)
            store_predictions(@predictions,user.id)
            erb :confirmation
        else
            session[:message] = "Invalid password"
            redirect "/prediction/new"
        end
    else
        session[:message] = "Invalid input"
        redirect "/prediction/new"
    end
end

get '/user/new' do
    @message = session.delete(:message) #stores an error message (if present) in @message to show to the user. It also deletes it from session.
    erb :'users/new'
end

post '/user/new' do
    username = params[:username]
    password = params[:password]
    if User.where(name: username).empty?
        User.create(name: username, password: password)
        erb :confirmation
    else
        session[:message] = "Username already in use!"
        redirect "/user/new"
    end
end

#Debug page where we can see all data
get '/submissions' do
    if ENV['APP_ENV']=="development" then
        @brackets = Bracket.all
        @predictions = Prediction.all
        erb :submissions
    else
        halt(404)
    end
end

#Summary page where we display anonymized valid data
get '/scores' do
    case Time.now
    when Time.parse("00:00:00 01-01-2010")..ROUND1_END
        treshold = 0
    when ROUND1_END..ROUND2_END
        treshold = 1
    when ROUND2_END..ROUND3_END
        treshold = 2
    when ROUND3_END..ROUND4_END
        treshold = 3
    else
        treshold = 4
    end
    @predictions = Prediction.where(valid: 1).where(Sequel.lit('round <= ?',treshold)).order(:round,:secret_no)
    @brackets = Bracket.where(valid: 1).order(:secret_no)
    erb :scores
end

#Handler for NotFound exceptions
not_found do
   "Page not found"
end
