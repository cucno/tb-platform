class Team < Sequel::Model
    include Comparable #essential to use <=>
    def <=>(other) #method to compare teams, by comparing the seeds
      @seed <=> other.seed
    end
end
