class Bracket < Sequel::Model
    one_to_many :brecords
    #upon creation of the bracket, assign it a random number
    def before_create
        super
        assign_unique_random_number
    end
    def compute_malus(other)
        malus = 0
        TEAMS.each { |team|
            #count how many times this bracket has that team passing the round (i.e. with 1 in a round). We have to use the method .round_dataset because the usual method .rounds gives an array, while .rounds_dataset gives an SQL dataset, and it's better to do the counting natively there than in a Ruby array
            occurrences = self.rounds_dataset.where(team.name.to_sym => 1).count
            other_occurrences = other.rounds_dataset.where(team.name.to_sym => 1).count
            difference = (occurrences - other_occurrences).abs
            malus += (1..difference).sum
        }
        return malus
    end
    #counts how many times 'team' appears in the rounds associated to the current bracket. We have to use .rounds_dataset instead of .rounds because the former gives a query (which we need) instead of an array of round objects.
    #number of times 'teams' appears = how many rounds it will pass = how far it goes in the playoffs. It's the metric to evaluate bracket malus.
    def count(team)
        return self.rounds_dataset.where(team => 1).count
    end
    private
    #assigns a random number to the bracket, ensuring it's not already used
    def assign_unique_random_number
        self.secret_no = loop do
            number = rand(0..99999)
            break number if Bracket[secret_no: number].nil?
        end
    end
end
