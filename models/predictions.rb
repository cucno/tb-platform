class Prediction < Sequel::Model
    many_to_one :user
    #We compute the malus allowing for a factor, which defaults to 1. Generally, different rounds will have different factors
    def compute_malus(other)
        malus = 0
        sweep_bonuses = 0
        weight = WEIGHT[round-1] #the factor/weight for the first round is WEIGHT[0]
        content_columns = Prediction.columns - [:id,:username,:secret_no,:round]
        content_columns.each { |team_column|
            #a column is either nil in both predictions or not-nil in both predictions, so it suffices to check this condition in 'self'.
            unless self[team_column].nil? then
                #we insert to_f on the denominator so that the result is also a float (we want float maluses)
                team_malus = (self[team_column] - other[team_column]).abs / 2.to_f
                if self.username != "reference" and team_malus == 0 and self[team_column]==0 and round > 1 then
                    sweep_bonuses += 1
                else
                    malus += team_malus
                end
            end
        }
        return malus*weight + sweep_bonuses*SWEEP_BONUS
    end
end
