require 'sequel'

if ENV['APP_ENV'] == "production" then
    DB = Sequel.sqlite('/data/torneo.db') #use persistent database in production
else
    DB = Sequel.sqlite('./torneo.db') #use other database in development
end

#In production the database is persistent, so we don't need to recreate the tables every time we load the file. If we try to do so, we get errors.

#Tabl
unless DB.table_exists?(:users)
    DB.create_table :users do
        primary_key :id
        column :name, String
        column :password, String, unique: true
    end
end

#Table for predictions. Every column is named after a team: in that column we will save the predicted number of wins for that team in the current round. If the team was already eliminated, the value remains nil. Order is irrelevant.
unless DB.table_exists?(:predictions)
    DB.create_table :predictions do
        primary_key :id
        column :team1, String
        column :team2, String
        column :wins1, Integer
        column :wins2, Integer
        column :valid, Integer, default: 1 #boolean that detects if the prediction is still valid (1) or has been replaced (0)
        column :round, Integer #we also keep track of the round at which we are predicting
        column :deadline, DateTime
        foreign_key :user_id, :users #every prediction is associated to a user
    end
end

#Table for bracket records. Every bracket is a collection of 16 pairs of (team, number of wins). In this table we store each of these 16 records separately. Every record is thus the id of a team, and the number of wins it is predicted to get + the foreign key to the user.
unless DB.table_exists?(:brecords)
    DB.create_table :brecords do
        primary_key :id
        column :team, String
        column :wins, Integer, default: 0
        foreign_key :user_id, :users #every bracket record is associated to a user
    end
end

unless DB.table_exists?(:teams)
    DB.create_table :teams do
        primary_key :id
        column :name, String
        column :seed, Integer
        column :conference, String
    end
end

#We then load the models (classes) for all objects
require_relative 'users'
require_relative 'predictions'
require_relative 'brecords'
require_relative 'teams'
